package com.fercugliandro.mastertech.auditfinanconsprod;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuditfinanConsProdApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuditfinanConsProdApplication.class, args);
	}

}
